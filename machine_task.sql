-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 05, 2022 at 10:05 AM
-- Server version: 10.4.22-MariaDB
-- PHP Version: 8.1.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `machine_task`
--

-- --------------------------------------------------------

--
-- Table structure for table `files`
--

CREATE TABLE `files` (
  `id` int(11) NOT NULL,
  `file_id` bigint(20) NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `file_name` varchar(500) NOT NULL,
  `mime_type` varchar(250) NOT NULL,
  `destination` varchar(250) NOT NULL,
  `path` varchar(250) NOT NULL,
  `size` bigint(20) NOT NULL,
  `field_name` varchar(250) NOT NULL,
  `original_name` varchar(250) NOT NULL,
  `encoding` varchar(250) NOT NULL,
  `meta` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL CHECK (json_valid(`meta`)),
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `files`
--

INSERT INTO `files` (`id`, `file_id`, `user_id`, `file_name`, `mime_type`, `destination`, `path`, `size`, `field_name`, `original_name`, `encoding`, `meta`, `created_at`, `updated_at`, `deleted_at`) VALUES
(4, 1, 2, 'a20ca4aa77eb9c8c218fe19ce7a3e299', 'image/png', 'uploads/files/', 'uploads\\files\\a20ca4aa77eb9c8c218fe19ce7a3e299', 269452, 'file', 'Screenshot (3).png', '7bit', '{\"revision\":0,\"created\":1646380738535,\"version\":0}', '2022-03-04 07:58:58', '2022-03-04 07:58:58', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `full_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `device_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1' COMMENT '1 for active, 0 for inactive',
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `profile_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `full_name`, `email`, `mobile_number`, `device_id`, `status`, `email_verified_at`, `password`, `profile_image`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'David Deo', 'email@gmail.com', '9999999999', NULL, '1', NULL, '$2b$10$LPNI5sz0q0EKsz0r7WA8vuneVR.Avfcx.t7vrXTbzTxvBlHkBT/2i', NULL, '2022-03-02 07:52:37', '2022-03-02 07:52:37', NULL),
(2, 'David Deo', 'emaissdl@gmail.com', '9989999999', NULL, '1', NULL, '$2b$10$xI2YEQ.HCPyBnuqvypp6uev9zx3MgsDrHv333ruG3VNJaDjwVPP9K', NULL, '2022-03-02 11:41:53', '2022-03-02 11:41:53', NULL),
(3, 'David Deo', 'emaissdl@gmadil.com', '9989699999', NULL, '1', NULL, '$2b$10$kTvbaOsdP2LWI.iUpr/4cu.qcybmG5LyAHy8QpEJyasSOXsxrE5ve', NULL, '2022-03-02 11:44:40', '2022-03-02 11:44:40', NULL),
(4, 'David Deo', 'emaissdl@sgmadil.com', '9189699999', NULL, '1', NULL, '$2b$10$.7NCh/kX51XVsKWVeSYPLex6KZzNDBbD8uM5gn3HgUXCf44M8HkKK', NULL, '2022-03-02 11:47:02', '2022-03-02 11:47:02', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `files`
--
ALTER TABLE `files`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_mobile_number_unique` (`mobile_number`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `files`
--
ALTER TABLE `files`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `files`
--
ALTER TABLE `files`
  ADD CONSTRAINT `files_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
