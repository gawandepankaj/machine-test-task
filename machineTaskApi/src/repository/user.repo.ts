import { AuthUser } from '../models/authuser.seq.model';
import { PasswordUtil } from '../utils/utils.bcrypt'
import JWTRedis from './../utils/auth.jwt'

var moment = require('moment');
const appConfig = require("../config/app.config");
const appError = require("../utils/appErrorMessage")
const apiStatus = require("../utils/restApiResponseStatus")
const logger = require("../utils/winston-logger");
//const messagequeue = require('../../../shared-utils/message-queue/message.helper.js');
const bcrypt = require('bcrypt');

export class UserRepo {

    /**
     *      
     * @param type 1 for mobile number, 2 for email and 3 for id 
     * @returns user info json
     * @description to get user details by mobile number.  
     */

    getUser(value: number, type = 1) {
        return new Promise((resolve, reject) => {

            let where = {};
            let response = {};
            switch (type) {
                case 1:
                    where = { mobile_number: value };
                    break;
                case 2:
                    where = { email: value };
                    break;
                default:
                    where = { mobile_number: value };
                    break;
            }

            AuthUser.findOne({
                attributes: ['id', 'full_name', 'email', 'password', 'mobile_number', 'created_at'],
                where: where,
                raw: true
            })
                .then(data => {
                    response = {
                        status: apiStatus.Unauthorized,
                        message: 'Great! you have registered successfully.',
                        data: null,
                        success: true
                    };
                    resolve(data);
                    return
                })
                .catch(err => {
                    logger.info("error in getUser", { message: JSON.stringify(err) });
                    reject(err)
                    return
                })
        });
    }

    login(user) {
        return new Promise((resolve, reject) => {
            let where: any = { status: '1' };
            if (user.mobile_number != undefined && user.mobile_number != "") {
                where.mobile_number = user.mobile_number;
            } else {
                where.email = user.email;
            }
            let jwt = new JWTRedis();
            AuthUser.findOne({
                attributes: ['id', 'full_name', 'email', 'password', 'mobile_number', 'status', 'created_at'],
                where: where,
                raw: true
            })
                .then(async data => {
                    if (data) {
                        let passwordUtil = new PasswordUtil(user.password)
                        const passwordIsValid = passwordUtil.compareHash(data.password);

                        if (passwordIsValid) {
                            delete data['password'];
                            let tokenInfo = await jwt.createToken(data);
                            data['token'] = tokenInfo.token;
                            data['refreshToken'] = tokenInfo.refreshToken;
                        } else {
                            data = null;
                        }
                    }
                    resolve(data);
                    return
                })
                .catch(err => {
                    logger.info("error in login", { message: JSON.stringify(err) });
                    reject(err)
                    return
                })
        });
    }

    postUser(userInfo: any) {
        return new Promise((resolve, reject) => {

            if (userInfo) {
                let jwt = new JWTRedis();
                let passwordUtil = new PasswordUtil(userInfo.password);
                userInfo.password = passwordUtil.getHash();

                AuthUser.create(userInfo)
                    .then(async user => {
                        let tokenInfo = await jwt.createToken(user);
                        user['token'] = tokenInfo.token;
                        user['refreshToken'] = tokenInfo.refreshToken;
                        resolve(user);
                        return
                    })
                    .catch(err => {
                        logger.info("ERROR postUser ", { message: JSON.stringify(err) });
                        reject(err)
                        return
                    })
            }
        });
    }

}