const { body, check } = require('express-validator')

exports.validate = (method) => {
  switch (method) {
    case 'register': {
      return [
        check('mobile_number').not().isEmpty().withMessage('The mobile number field is required.'),
        check('mobile_number').isLength({ min: 10, max: 10 }).withMessage('Mobile number should contains 10 digits.'),
        check('full_name').not().isEmpty().withMessage('The first name field is required.'),
        check('email').not().isEmpty().withMessage('The email field is required.'),
        check('password').not().isEmpty().withMessage('The password field is required.'),
        check('password').isLength({ min: 6, max: 32 }).withMessage('Password should have atleast 6 characters')
      ]
    }
    case 'login': {
      return [
        //check('mobile_number').isLength({ min: 10, max: 10 }).withMessage('Mobile number should contains 10 digits.'),
        //check('mobile_number').not().isEmpty().withMessage('The mobile number field is required.'),
        //check('email').not().isEmpty().withMessage('The email field is required.'),
        check('password').not().isEmpty().withMessage('The password field is required.'),
      ]
    }
    case 'file': {
      return [
        check('file').not().isEmpty().withMessage('The file field is required.'),
      ]
    }
  }
}