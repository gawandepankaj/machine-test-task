module.exports = {
    TOKENSECRET: "PharmaHire2021",
    TOKENEXPIRY: 43800, // 1 month
    REFRESHTOKENEXPIRY: 86400, // 24 hr
    REDISPORT: 6397,
    FROMEMAIL: 'hello@covermyday.com',
    AllowedIPs : ["127.0.0.1","::ffff:127.0.0.1"],
    RESETPASSWORDTOKENEXPIRY: 3600,
    RESETPASSWORDLINK : 'https://app.covermyday.com/passwordreset',
    FORGOTPASSWORDLINK : 'https://app.covermyday.com/forgotpassword',
    SETPASSWORDLINK : 'https://app.covermyday.com/setpassword',
    APPCLIENTID : 'ph_005001yuib2020ZXCVB'
};