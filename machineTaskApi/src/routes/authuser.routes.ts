import { Express } from 'express';
import * as multer from 'multer'
const validateToken = require("../middleware/auth.jwt.middleware")
const user = require("../controllers/user.controller");
const UserRequest = require('../requests/user.request');
const FileController = require('../controllers/file.controller')
const UPLOAD_PATH = 'uploads/files';
const upload = multer({ dest: `${UPLOAD_PATH}/` });

export function routes(app: Express) {
    app.post('/api/register', UserRequest.validate('register'), user.register);
    app.post('/api/login', UserRequest.validate('login'), user.login);
    app.post('/api/file/upload', upload.single('file'), [validateToken], FileController.uploadFile);
    app.get('/api/file/list', [validateToken], FileController.list);
    app.get('/api/file/detail/:id', [validateToken], FileController.getFile);
}