import * as winston from 'winston'
import { Express, Request, Response } from 'express'
const AuthUserRoutes = require('./authuser.routes.js');
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('../../swagger/swagger.json');
const basicAuth = require('express-basic-auth');

const swaggerCSS = {
  customCss: 'swagger-ui .topbar { display: none }',
  customSiteTitle: "Machine Task",
  // customfavIcon: "./dry.png"
};
export function initRoutes(app: Express) {
  winston.log('info', '--> Initialisations des routes')
  //Swagger
  app.use('/api-docs', basicAuth({
    users: { 'machinetask': 'pankaj123' },
    challenge: true
  }), swaggerUi.serve, swaggerUi.setup(swaggerDocument, swaggerCSS));
  app.get('/api', (req: Request, res: Response) => res.status(200).send({
    message: 'server is listening!'
  }))
  AuthUserRoutes.routes(app);
  app.all('*', (req: Request, res: Response) => res.status(404).send())
}