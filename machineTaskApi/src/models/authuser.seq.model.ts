import { Model, DataTypes } from 'sequelize'
import sequelize from './db.sequelize'

export class AuthUser extends Model {
  password(password: any) {
    throw new Error('Method not implemented.');
  }
}
AuthUser.init(
  {
    id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      field: 'id'
    },
    full_name: {
      type: DataTypes.STRING(45),
      allowNull: true,
      field: 'full_name'
    },
    email: {
      type: DataTypes.STRING(50),
      allowNull: true,
      field: 'email',
      unique: true
    },
    password: {
      type: DataTypes.STRING(100),
      allowNull: true,
      field: 'password'
    },
    device_id: {
      type: DataTypes.STRING(255),
      allowNull: true,
      field: 'device_id'
    },
    mobile_number: {
      type: DataTypes.STRING(20),
      allowNull: true,
      field: 'mobile_number',
      unique: true
    },
    profile_image: {
      type: DataTypes.STRING(255),
      allowNull: true,
      field: 'profile_image'
    },
    status: {
      type: DataTypes.ENUM,
      values: ['0', '1'],
      defaultValue: '1',
      field: 'status'
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: true,
      field: 'created_at'
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: true,
      field: 'updated_at'
    },
    deletedAt: {
      type: DataTypes.DATE,
      allowNull: true,
      field: 'deleted_at'
    }
  },
  { sequelize, freezeTableName: true, modelName: 'users' }
);
