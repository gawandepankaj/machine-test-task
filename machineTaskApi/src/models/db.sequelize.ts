import {Sequelize} from 'sequelize';
const seqConfig = require("../config/db.config");

const sequelize = new Sequelize(
    seqConfig.DB,
    seqConfig.USER,
    seqConfig.PASSWORD,
  {
    host: seqConfig.HOST,
    dialect: "mysql",
    logging: false,
    timezone: "+00:00",
    pool: seqConfig.pool,
    port: seqConfig.PORT
  }
)
sequelize.authenticate().then(function(errors) { 
  //console.log(errors) 
});
export default sequelize

