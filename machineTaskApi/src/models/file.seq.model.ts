import { Model, DataTypes } from 'sequelize'
import sequelize from './db.sequelize'

export class File extends Model {
}
File.init(
    {
        id: {
            type: DataTypes.INTEGER,
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            field: 'id'
        },
        file_id: {
            type: DataTypes.INTEGER,
            allowNull: true,
            field: 'file_id'
        },
        user_id: {
            type: DataTypes.BIGINT,
            allowNull: true,
            references: {
                model: 'users',
                key: 'id'
            },
            field: 'user_id'
        },
        file_name: {
            type: DataTypes.STRING,
            allowNull: true,
            field: 'file_name'
        },
        mime_type: {
            type: DataTypes.STRING,
            allowNull: true,
            field: 'mime_type'
        },
        destination: {
            type: DataTypes.STRING,
            allowNull: true,
            field: 'destination'
        },
        path: {
            type: DataTypes.STRING,
            allowNull: true,
            field: 'path'
        },
        size: {
            type: DataTypes.BIGINT,
            allowNull: true,
            field: 'size'
        },
        field_name: {
            type: DataTypes.STRING,
            allowNull: true,
            field: 'field_name'
        },
        original_name: {
            type: DataTypes.STRING,
            allowNull: true,
            field: 'original_name'
        },
        encoding: {
            type: DataTypes.STRING,
            allowNull: true,
            field: 'encoding'
        },
        meta: {
            type: DataTypes.JSON,
            allowNull: true,
            field: 'meta'
        },
        createdAt: {
            type: DataTypes.DATE,
            allowNull: true,
            field: 'created_at'
        },
        updatedAt: {
            type: DataTypes.DATE,
            allowNull: true,
            field: 'updated_at'
        },
        deletedAt: {
            type: DataTypes.DATE,
            allowNull: true,
            field: 'deleted_at'
        }
    },
    { sequelize, freezeTableName: true, modelName: 'files' }
);
