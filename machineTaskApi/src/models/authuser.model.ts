const sql = require("./db");

const User = function (users) {
  this.id = users.id
  this.full_name = users.full_name
  this.email = users.email
  this.password = users.password
  this.mobile_number = users.mobile_number
  this.status = users.status
  this.profile_image = users.profile_image,
    this.created_at = users.created_at
  this.updated_at = users.updated_at
  this.deleted_at = users.deleted_at
};

module.exports = User;