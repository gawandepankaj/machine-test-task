
const appErrorMessage = {  
      
   NODATA: 'Content cannot be empty.', 
   EMAILALREADYEXISTS: 'Email address already exists.', 
   EXCEPTION: 'Error occured in application.',
   INVALIDLOGIN:'Incorrect username or password.',
   INVALIDTOKEN: 'Your existing session token is not valid',
   CODENOTMATCHED: 'The added code does not match',
   DATANOTFOUND: 'Record not found',
   EMAILDOESNOTEXISTS : 'Email address does not exists',
   RESETPASSWORDLINKEXPIRED: 'Reset Password link expired',
   SUCCESS: 'Success',
   OLDPASSWORDDOSENOTMATCH: 'Entered Old Password does not match.',
   NORECORDWITHEMAIL: 'No record found for the given Email Id'   
}
                           
module.exports = appErrorMessage