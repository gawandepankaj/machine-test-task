const restApiResponseStatus = {
    OK: 200, 
    BadRequest: 400,
    Unauthorized: 401, 
    Forbidden: 403,
    NotFound: 404,
    InternalServerError: 500
}

module.exports = restApiResponseStatus