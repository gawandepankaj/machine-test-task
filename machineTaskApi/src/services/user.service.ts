const logger = require("./../utils/winston-logger");
import { UserRepo } from '../repository/user.repo'

const appConfig = require("../config/app.config");
const appError = require("../utils/appErrorMessage")
const apiStatus = require("../utils/restApiResponseStatus")

exports.initiateResetPassword = async (req, res) => {

    var ip = req.ip || req.connection.remoteAddress || req.socket.remoteAddress || req.connection.socket.remoteAddress;

    logger.info('ip', { message: ip });

    if (appConfig.AllowedIPs.indexOf(ip) == -1) {
        return res.status(apiStatus.Forbidden).send("Forbidden");
    }

    if (!req.body) {
        return res.status(apiStatus.BadRequest).send(appError.NODATA);
    }

    const userRepo = new UserRepo()
}