import * as winston from 'winston'
import JWTRedis from './../utils/auth.jwt'


const appConfig = require("../config/app.config");
const appError = require("../utils/appErrorMessage")
const apiStatus = require("../utils/restApiResponseStatus")

module.exports = function (req, res, next) {

    let token = req.headers['x-access-token'] || req.headers['authorization'];
    if (token) {
        token = token.replace(/^Bearer\s/, '');
    }

    if (token) {
        let jwt = new JWTRedis()
        jwt.verifyToken(token)
            .then(_decoded => {
                if (_decoded) {
                    req.user = _decoded;
                    return next();
                } else {
                    return res.status(apiStatus.Unauthorized).send(appError.INVALIDTOKEN)
                }
            })
            .catch(err => {
                return res.status(apiStatus.InternalServerError).send(appError.EXCEPTION)
            })
    } else {
        return res.status(apiStatus.Unauthorized).send(appError.INVALIDTOKEN)
    }
};