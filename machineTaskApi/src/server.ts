import app from "./app";
const logger = require("./utils/winston-logger");
require('dotenv').config()

const cors = require("cors");

const PORT = process.env.PORT || 3001;

var corsOptions = {
    origin: "*"
  };

app.use(cors(corsOptions));

app.listen(PORT, () => logger.info(`App listening on port ${PORT}!`));