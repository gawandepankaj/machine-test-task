const { check, validationResult } = require('express-validator');
import { UserRepo } from "../repository/user.repo";
const User = require("../models/authuser.model");
const apiStatus = require("../utils/restApiResponseStatus");
const appError = require("../utils/appErrorMessage");

exports.register = async (req, res) => {
    try {
        const errors = validationResult(req);

        if (!errors.isEmpty()) {
            return res.status(422).json({ errors: errors.array() })
        }
        var userInfo = new User(req.body);
        const userRepo = new UserRepo();
        let isMobileNumberExists = await userRepo.getUser(req.body.mobile_number);
        let isEmailExists;
        if (req.body.email != undefined && req.body.email != "") {
            isEmailExists = await userRepo.getUser(req.body.email, 2);
        }
        let response = {};
        if (isMobileNumberExists != undefined && isMobileNumberExists != "") {
            response = { success: false, status: 409, message: 'This mobile number already registered with us.' };
        } else if (isEmailExists != undefined && isEmailExists != "") {
            response = { success: false, status: 409, message: 'This email already registered with us.' };
        } else {
            await userRepo.postUser(userInfo);
            const getUser = await userRepo.getUser(req.body.mobile_number);
            response = { success: true, status: 200, message: 'User has been successfully registered.' }
        }
        res.status(200).json(response);
    } catch (error) {
        console.log("Error: ", error);
        res.status(500).json({ error: error });
    }
};

exports.login = async (req, res,) => {
    try {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(422).json({ errors: errors.array() })
        }
        const userRepo = new UserRepo();
        const userInfo = await userRepo.login(req.body);
        let response = {};
        if (userInfo) {
            response = {
                status: apiStatus.OK,
                success: true,
                data: userInfo,
                message: 'You have login successfully.'
            };
        } else {
            response = {
                status: apiStatus.Unauthorized,
                success: false,
                message: appError.INVALIDLOGIN
            };
        }
        res.status(200).send(response);
    } catch (err) {
        console.log("Err: ", err);
        res.status(500).json({ error: err });
    }
};
