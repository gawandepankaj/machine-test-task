import * as Loki from 'lokijs'
import { loadCollection } from '../utils/utils';
import * as fs from 'fs'
import * as path from 'path'
const { check, validationResult } = require('express-validator');
const DB_NAME = 'db.json';
const COLLECTION_NAME = 'files';
const UPLOAD_PATH = 'uploads/files';
const db = new Loki(`${UPLOAD_PATH}/${DB_NAME}`, { persistenceMethod: 'fs' });

exports.uploadFile = async (req, res) => {
    try {
        // const errors = validationResult(req);
        // if (!errors.isEmpty()) {
        //     return res.status(422).json({ errors: errors.array() })
        // }
        const user_id = req.user.id;
        const col = await loadCollection(COLLECTION_NAME, db);
        const data = col.insert({ ...req.file, ...{ user_id } });
        const result = col.findOne({ '$loki': data.$loki });
        db.saveDatabase();
        res.send({ status: 200, success: true, message: 'File successfully uploaded', data: result });
    } catch (error) {
        console.log("Error: ", error);
        res.status(400).json({ error: error });
    }
};

exports.list = async (req, res) => {
    try {
        let response = {};
        const col = await loadCollection(COLLECTION_NAME, db);
        const user = req.user;
        const user_id = user.id;
        const list = col.find({ user_id });
        const listArray = Object.keys(list).length
        if (listArray != 0) {
            response = { success: true, status: 200, message: 'File list is found', data: list }
        } else {
            response = { success: false, status: 404, message: 'File list not found' }
        }
        res.send(response);
    } catch (error) {
        console.log("Error: ", error);
        res.status(400).json({ error: error });
    }
};

exports.getFile = async (req, res) => {
    try {
        const col = await loadCollection(COLLECTION_NAME, db);
        const result = col.get(req.params.id);
        if (!result) {
            res.sendStatus(404);
            return;
        };
        res.setHeader('Content-Type', result.mimetype);
        fs.createReadStream(path.join(UPLOAD_PATH, result.filename)).pipe(res);
    } catch (error) {
        console.log("Error: ", error);
        res.status(400).json({ error: error });
    }
}