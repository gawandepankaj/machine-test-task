
export interface userManagementDefaultState {
    userlist: userItem[],
    totalRecords: number
}

export interface userItem {
    id: string;
    full_name: string;
    email: string;
    mobile_number: number;
    status: Boolean;
    refreshToken: Boolean;
    token: Boolean;
    user_type: Boolean;
}