
export interface fileDefaultState {
    fileList: fileItem[],
    totalRecords: number
}

export interface fileItem {
    id: string;
    file_id: string;
    file_name: string;
    mime_type: string;
    destination: string;
    size: string;
    field_name: string;
    original_name: string;
    status: string;    
    created_at: string;    
}
