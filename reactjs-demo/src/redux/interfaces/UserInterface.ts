
export interface userDefaultState {
    userdata: [],
    userDetails: any;
    userList: userItem[],
    totalRecords: number,
    isUserLogin: boolean
}

export interface userPage {
    count: number,
    rows: []    
}

export interface userItem {   
    id: string;
    full_name: string;
    mobile_number: string;
    email: string;
    status:string,
    created_at: string
    user_type: string     
}