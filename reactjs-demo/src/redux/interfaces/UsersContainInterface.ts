
export interface UsersDefaultState {
    UsersList: UsersItem[],
    totalRecords: number
}

export interface UsersItem {
    id: string;
    first_name: string;
    last_name: string;
    email: string;    
}