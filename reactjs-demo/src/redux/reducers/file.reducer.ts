import { createSlice, PayloadAction } from "@reduxjs/toolkit";

import { fileDefaultState, fileItem } from "../interfaces/FileInterface";
import { AppState } from "../store";

const initialState: fileDefaultState = {
    fileList: [],
    totalRecords: 0
};

export const fileReducer = createSlice({
  name: "fileState",
  initialState,
  reducers: {
    setFileListData: ( state, action: PayloadAction<fileItem[]> ) => ({
      ...state,
      fileList: action.payload,
    }),        
  },
});

export const { setFileListData } = fileReducer.actions;

export const getFileListData = (state: AppState) => state.fileReducer.fileList;

export { initialState as fileState };
export default fileReducer;
