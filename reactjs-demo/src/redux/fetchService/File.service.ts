
import ApiEndPoints from "../../Network_call/ApiEndPoints";
import ApiServices from "../../Network_call/apiservices";

export const getFiles = async (payload: any) => {    
    return await ApiServices('get', payload, ApiEndPoints.FILES);    
};

export const UploadFileApi = async (payload: any) => {
    return await ApiServices('post', payload, ApiEndPoints.UploadFile);    
};
