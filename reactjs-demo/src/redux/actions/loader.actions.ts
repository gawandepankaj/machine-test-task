import { AppThunk } from "../store";
import { setLoading } from "../reducers/loader.reducer";

export const LoaderAction = (payload: any): AppThunk => async (dispatch: any) => {
    dispatch(setLoading(payload));  
};