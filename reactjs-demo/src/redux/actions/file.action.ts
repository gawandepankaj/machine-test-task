import { AppThunk } from "../store";
import { toast } from "react-toastify";
import { setFileListData } from "../reducers/file.reducer";
import { getFiles, UploadFileApi } from "../fetchService/File.service";

export const FileListAction = (payload: any): AppThunk => async (dispatch: any) => {

    const response: any = await getFiles(payload);
    if (response.data) {
        dispatch(setFileListData(response.data));
    }
};

export const setFileData = (payload: any, navigate: any): AppThunk => async (dispatch: any) => {

    const response: any = await UploadFileApi(payload);
    toast(response.message);
    if (response) {
        navigate('/files');
    }
};


