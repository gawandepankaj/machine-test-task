import { createLogger } from "redux-logger";
import { configureStore, ThunkAction, Action } from "@reduxjs/toolkit";
import userManagementReducer from "./reducers/userManagementReducer";
import fileReducer from "./reducers/file.reducer";
import loaderReducer from "./reducers/loader.reducer";
import userReducer from "./reducers/user.reducer";
import UserReducer from "./reducers/UserReducer";


const logger = createLogger();

export const store = configureStore({
  reducer: {
    userManagement: userManagementReducer.reducer,
    loaderReducer: loaderReducer.reducer,
    userReducer: userReducer.reducer,
    UserReducer: UserReducer .reducer,
    fileReducer: fileReducer.reducer,
  },
  middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat(logger),
});

export type AppState = ReturnType<typeof store.getState>;

export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  AppState,
  unknown,
  Action<string>
>;

export default store;

export type commonState = ReturnType<typeof store.getState>

export type AppDispatch = typeof store.dispatch