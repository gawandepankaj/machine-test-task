import React from 'react';
import { BrowserRouter, Route, Routes} from 'react-router-dom';
import Login from '../pages/auth/login';

const PublicRouter = () => {
    return (
        <BrowserRouter>            
            <Routes>
                <Route path="/" element={<Login />} />                                    
                <Route path="/login" element={<Login />} />                
            </Routes>
        </BrowserRouter>
    );
}

export default PublicRouter;