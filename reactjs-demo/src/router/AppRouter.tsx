import React from 'react';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import Home from '../pages/home';
import Footer from '../pages/layout/footer';
import Header from '../pages/layout/header';
import FileList from '../pages/file/list';

const AppRouter = () => {
    return (
        <BrowserRouter>
            <Header />
            <Routes>
                <Route path="/" element={<Home />} />
                <Route path="/home" element={<Home />} />
            
                <Route path="/files">
                    <Route index={true} element={<FileList />} />
                    {/* <Route index={false} path="upload" element={<UploadFile />} /> */}
                </Route>

                <Route path="*" element={<Home />} />
            </Routes>
            <Footer />

        </BrowserRouter>
    );
}

export default AppRouter;