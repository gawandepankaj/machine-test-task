
const APPConfig = {  
  API_URL: "http://localhost:3001/api"  
}
const ApiEndPoints = {
  Login: `${APPConfig.API_URL}/login`,
  AreaList: `${APPConfig.API_URL}/area/list`,
  TagBatchList: `${APPConfig.API_URL}/tag-batch/list`,
  AddTagBatch: `${APPConfig.API_URL}/tag-batch/add`,
  TagBatchDetails: `${APPConfig.API_URL}/tag-batch/detail`,
  CityList: `${APPConfig.API_URL}/city/list`,
  CityAdd: `${APPConfig.API_URL}/city/add`,
  CountryList: `${APPConfig.API_URL}/country/list`,
  CountryAdd: `${APPConfig.API_URL}/country/add`,
  CountryUpdate: `${APPConfig.API_URL}/country/update`,
  DriverList: `${APPConfig.API_URL}/driver/list`,
  DriverAdd: `${APPConfig.API_URL}/driver/add`,
  DriverUpdate: `${APPConfig.API_URL}/user/update`,
  DriverDetails: `${APPConfig.API_URL}/driver/detail`,
  LaundryList: `${APPConfig.API_URL}/laundry/list`,
  LaundryAdd: `${APPConfig.API_URL}/laundry/add`,
  LaundryUpdate: `${APPConfig.API_URL}/laundry/update`,
  StateList: `${APPConfig.API_URL}/state/list`,
  StateAdd: `${APPConfig.API_URL}/state/add`,
  StateUpdate: `${APPConfig.API_URL}/state/update`,
  StatusUpdate: `${APPConfig.API_URL}/status`,
  StatusDelete: `${APPConfig.API_URL}/delete`,
  StripeSubscriptionPlans: `${APPConfig.API_URL}/stripe/subscription-plans`,
  UserList: `${APPConfig.API_URL}/user/list`,
  UserAdd: `${APPConfig.API_URL}/user/add`,
  UserUpdate: `${APPConfig.API_URL}/user/update`,
  UserUpdate: `${APPConfig.API_URL}/user/update`,
  UserDetails: `${APPConfig.API_URL}/user/detail/`,
  AddZipcode: `${APPConfig.API_URL}/locality/add`,
  ListZipcode: `${APPConfig.API_URL}/locality/list`,
  UpdateZipcode: `${APPConfig.API_URL}/locality/update`,
  AddArea: `${APPConfig.API_URL}/area/add`,
  UpdateArea: `${APPConfig.API_URL}/area/update`,
  AreaDetails: `${APPConfig.API_URL}/area/detail`,
  InterestedZipcodeList: `${APPConfig.API_URL}/interested-zipcode/list`,
  ApartmentList: `${APPConfig.API_URL}/apartment/list`,
  ApartmentAdd: `${APPConfig.API_URL}/apartment/add`,
  ApartmentUpdate: `${APPConfig.API_URL}/apartment/update`,
  TagList: `${APPConfig.API_URL}/tag/list`,
  DispatchTagList: `${APPConfig.API_URL}/dispatchTag/list`,

  FILES: `${APPConfig.API_URL}/file/list`,
  UploadFile: `${APPConfig.API_URL}/file/upload`,
}

export default ApiEndPoints

