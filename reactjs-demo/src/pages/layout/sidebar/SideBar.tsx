import * as React from 'react';
import { Link } from "react-router-dom";
import { Accordion } from 'react-bootstrap';

const SideBar = () => {
    return (
        <ul id="dc_accordion" className="sidebar-menu tree">
            <li>
                <Link to="/home">
                    <i className="fa fa-home"></i>
                    <span>Dashboard</span>
                </Link>
            </li>
            <li className="menu_sub">

                <div id='accordion' className='accordion drop_custm'>                    
                    
                    <Link to="/files">
                        <i className="fa fa-map-signs"></i>
                        <span>Files</span>
                    </Link>
                                                            
                </div>

            </li>
        </ul>
    );
}

export default SideBar;