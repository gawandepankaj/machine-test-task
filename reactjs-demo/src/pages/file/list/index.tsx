import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import SideBar from '../../layout/sidebar/SideBar';
import BreadCrum from '../../layout';
import { Link } from 'react-router-dom'
import { DateTime } from '../../../common/components/Date';
import DataTable from 'react-data-table-component';
import { LoaderBullet } from '../../../common/components/Loader';
import { getLoading } from '../../../redux/reducers/loader.reducer'
import { getFileListData } from '../../../redux/reducers/file.reducer';
import { fileItem } from '../../../redux/interfaces/FileInterface';
import { FileListAction } from '../../../redux/actions/file.action';

function FileList() {

  const dispatch = useDispatch();
  const files = useSelector(getFileListData);
  const loader = useSelector(getLoading);

  const [fileData, setFileData] = useState<fileItem[]>([]);
  const [loading, setLoading] = useState(false);
  const [totalRows, setTotalRows] = useState(0);

  const columns: any = [
    {
      name: '#',
      selector: (row: any, id: number) => id + 1,
    },
    {
      name: 'filename',
      selector: (row: any) => row.filename
    },
    {
      name: 'mimetype',
      selector: (row: any) => row.filename
    },
    {
      name: 'filename',
      selector: (row: any) => row.mimetype
    },
    {
      name: 'size',
      selector: (row: any) => row.size
    },    
    {
      name: 'Created At',
      selector: (row: any) => row.created_at,
      cell: (row: any) => <DateTime date={row.created_at} />
    },
    {
      name: 'Action',
      cell: (row: any) => {
        let id = row.id;
        return (
          <div className="dropdown">
            <button className="btn btn-secondary dropdown-toggle"
              type="button"
              id="dropdownMenuButton"
              data-toggle="dropdown"
              aria-haspopup="true"
              aria-expanded="false"
            >
              Action
            </button>
            <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
              <Link
                className="dropdown-item" to={`edit/${row.id}`}
                state={{ areaInfo: row }}
              >Edit</Link>              

            </div>
          </div>
        );
      },
    },
  ];


  useEffect(() => {
    setLoading(loader);
  }, [loader]);

  const fetchData = async () => {
    let payload = {
      page: 1
    }
    dispatch(FileListAction(payload));
  };

  const handlePageChange = (page: number) => {
    //fetchUsers(page);
  };

  const handlePerRowsChange = async (newPerPage: number, page: number) => {
    fetchData();
  };

  useEffect(() => {
    fetchData();
    return () => { };
  }, [dispatch]);

  useEffect(() => {
    setFileData(files);    
  }, [files]);

  

  return (
    <div className="container_full">
      <div className="side_bar scroll_auto">
        <SideBar />
      </div>
      <div className="content_wrapper">
        <div className="container-fluid mb-5">
          <BreadCrum title={'Files'} />
          <div className='text-right mb-2'>
            <Link className="btn btn-primary" to={'add'} >Upload New File</Link>
          </div>
          <DataTable
            title="Files"
            columns={columns}
            data={fileData}
            progressPending={loading}
            pagination            
            paginationTotalRows={totalRows}
            onChangeRowsPerPage={handlePerRowsChange}
            onChangePage={handlePageChange}
            progressComponent={<LoaderBullet />}
          />
        </div>
      </div>
    </div>
  )
}

export default FileList;