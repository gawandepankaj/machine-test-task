import * as React from 'react';

const Status = ({ status }: { status: any }) => {

    return (
        status === "1" ? <span className="badge badge-pill badge-success">Active</span> : <span className="badge badge-pill badge-warning">Inactive</span>
    );
}

export default Status;