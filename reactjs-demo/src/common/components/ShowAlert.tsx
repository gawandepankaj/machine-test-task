import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css'

const ShowAlert = (res: { message: string, data: any, status: any }) => {

  try {

    switch (res.status) {
      case 200:
        toast.success(res.message, {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
        break;

      case 500:
        toast.warn(res.message, {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });

        break;
      default:
        toast('I am default.');
        break;
    }
  } catch (error) {
    console.log(error);
  }
}

export { ShowAlert };