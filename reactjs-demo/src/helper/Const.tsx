const Const = {
    DATETIME: 'D, MMM YYYY hh:mm',
    DATE: 'D, MMM YYYY',
    TAGTYPE: { 1: "laundry", 2: "dryCleaning" }
}

export default Const;